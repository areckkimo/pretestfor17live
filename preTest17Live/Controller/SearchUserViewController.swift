//
//  SearchUserViewController.swift
//  preTest17Live
//
//  Created by Eric Chen on 2021/5/28.
//

import UIKit

class SearchUserViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var keywordTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    var usersViewModel: UsersViewModel?
    let loadingViewID = "LoadingView"
    let userCellID = "UserCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: "LoadingView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: loadingViewID)
    }
    
    //MARK: - IBAction
    @IBAction func searchOnPressed(_ sender: UIButton?) {
        
        keywordTextField.resignFirstResponder()
        usersViewModel = nil
        collectionView.reloadData()
        
        let keyword = keywordTextField.text ?? ""
        let request = UserRequest.search(keyword: keyword)
        usersViewModel = UsersViewModel(request: request, delegate: self)
        usersViewModel?.fetchUser()
    }
    @IBAction func keywordValueOnChange(_ sender: UITextField) {
        if sender == keywordTextField, let keyword = sender.text {
            searchButton.isEnabled = (!keyword.isEmpty) ? true : false
        }
    }
    
}

extension SearchUserViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return usersViewModel?.currentCount ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: userCellID, for: indexPath) as! UserCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let loadingView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: loadingViewID, for: indexPath) as! LoadingView
        
        if usersViewModel?.isLastPage ?? false {
            loadingView.activityIndicator.stopAnimating()
            loadingView.statusLabel.text = "已經到底了"
        }else{
            loadingView.activityIndicator.startAnimating()
            loadingView.statusLabel.text = "載入中"
        }
        
        return loadingView
    }
}

extension SearchUserViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let user = usersViewModel?.user(at: indexPath.item) {
            let userCell = cell as! UserCell
            userCell.config(user: user, indexPath: indexPath, collectionView: collectionView)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.frame.width / 2 - 10
        return CGSize(width: cellWidth, height: cellWidth + 38)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if usersViewModel?.currentCount ?? 0 > 0 || usersViewModel?.isFetching ?? false {
            return CGSize(width: collectionView.frame.size.width, height: 50)
        }
        return .zero
    }
}

extension SearchUserViewController: UsersViewModelDelegate{
    func onFetchCompleted() {
        collectionView.reloadData()
    }
    
    func onFetchFaild(error: HTTPError) {
        print(error.description)
    }
}

extension SearchUserViewController: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollViewContentOffsetMaxY = scrollView.contentSize.height - scrollView.frame.size.height
        let scrollViewContentOffsetY = scrollView.contentOffset.y
        if scrollViewContentOffsetY > scrollViewContentOffsetMaxY, let viewModel = usersViewModel {
            if !viewModel.isLastPage {
                //load more data
                usersViewModel?.fetchUser()
            }
        }
    }
}

extension SearchUserViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        searchOnPressed(nil)
        return true
    }
}
