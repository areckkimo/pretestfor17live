//
//  UserViewModel.swift
//  preTest17Live
//
//  Created by Eric Chen on 2021/5/28.
//

import Foundation

protocol UsersViewModelDelegate: AnyObject {
    func onFetchCompleted()
    func onFetchFaild(error: HTTPError)
}

final class UsersViewModel {
    
    private weak var delegate: UsersViewModelDelegate?
    private var users = [User]()
    private var currentPage = 0
    private var totalPages = 1
    private var isFetchInProgress = false
    
    private let gitHubClient = GitHubClient()
    private let request: UserRequest
    
    init(request: UserRequest, delegate: UsersViewModelDelegate) {
        self.request = request
        self.delegate = delegate
    }
    
    var currentCount: Int {
        return users.count
    }
    
    var isLastPage: Bool {
        return currentPage == totalPages
    }
    
    var isFetching: Bool {
        return isFetchInProgress
    }
    
    func user(at index: Int) -> User {
        return users[index]
    }
    
    func fetchUser() {
        
        if isFetchInProgress {
            return
        }
        
        isFetchInProgress = true
        
        let nextPage = currentPage + 1
        print("nextPage: \(nextPage)")
        
        gitHubClient.fetchUsers(with: request, page: nextPage) { (result) in
            switch result {
            case .failure(let error):
                self.isFetchInProgress = false
                self.delegate?.onFetchFaild(error: error)
            case .success((let data, let response)):
                self.currentPage += 1
                if self.currentPage == 1, let lastLink = response.findLink(["rel" : "last"]), let lastLinkComponents = URLComponents(string: lastLink.uri) {
                    if let page = lastLinkComponents.findQueryValue(with: "page") {
                        self.totalPages = Int(page) ?? 0
                    }
                    print("totalPages: \(self.totalPages)")
                }
                self.isFetchInProgress = false
                self.users.append(contentsOf: data.users)
                DispatchQueue.main.async {
                    self.delegate?.onFetchCompleted()
                }
            }
        }
    }
}
