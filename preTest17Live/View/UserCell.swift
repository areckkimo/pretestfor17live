//
//  UserCell.swift
//  preTest17Live
//
//  Created by Eric Chen on 2021/5/28.
//

import UIKit

class UserCell: UICollectionViewCell {
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    func config(user: User, indexPath: IndexPath, collectionView: UICollectionView){
        nameLabel.text = user.login
        avatarImageView.image = nil
        guard let avatarURL = URL(string: user.avatarURL) else{
            return
        }
        PhotoDownloadManager.shared.downloadPhoto(with: avatarURL, indexPath: indexPath) { (result) in
            switch result {
            case .success(let (image, indexPath)):
                DispatchQueue.main.async {
                    if let cell = collectionView.cellForItem(at: indexPath) as? UserCell {
                        cell.avatarImageView.image = image
                    }
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
