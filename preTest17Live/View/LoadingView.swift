//
//  LoadingView.swift
//  preTest17Live
//
//  Created by Eric Chen on 2021/5/28.
//

import UIKit

class LoadingView: UICollectionReusableView {
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
}
