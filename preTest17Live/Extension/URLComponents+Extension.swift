//
//  URLComponents+Extension.swift
//  preTest17Live
//
//  Created by Eric Chen on 2021/5/28.
//

import Foundation

extension URLComponents {
    func findQueryValue(with queryName: String) -> String? {
        
        guard let queryItems = queryItems else {
            return ""
        }
        
        for queryItem in queryItems {
            if queryItem.name == queryName {
                return queryItem.value ?? ""
            }
        }
        
        return ""
    }
}
