//
//  WebLinkingParser.swift
//  preTest17Live
//
//  Created by Eric Chen on 2021/5/27.
//

import Foundation

struct Link {
    let uri: String
    let attribute: [String: String]
}

extension HTTPURLResponse{
    var links: [Link] {
        if let linkHeaders = allHeaderFields["Link"] as? String {
            return parseLink(linkHeaders: linkHeaders)
        }
        return []
    }
    
    func findLink(_ attribute: [String: String]) -> Link? {
        for link in links {
            if link.attribute ~= attribute {
                return link
            }
        }
        return nil
    }
}

func parseLink(linkHeaders: String) -> [Link] {
    
    return linkHeaders.components(separatedBy: ",").map { (linkHeader) -> Link in
        let linkHeaderComponents = linkHeader.components(separatedBy: "; ")
        let uri = linkHeaderComponents[0].trimmingCharacters(in: [" ","<",">"])
        
        let attributeString = linkHeaderComponents[1]
        let attributeComponents = attributeString.components(separatedBy: "=")
        let attributeName = attributeComponents[0]
        let attributeValue = attributeComponents[1].trimmingCharacters(in: ["\""])
        let attribute = [attributeName: attributeValue]
        
        return Link(uri: uri, attribute: attribute)
    }
    
}

func ~=(lhs: [String: String], rhs: [String: String]) -> Bool {
  for (key, value) in rhs {
    if lhs[key] != value {
      return false
    }
  }

  return true
}

