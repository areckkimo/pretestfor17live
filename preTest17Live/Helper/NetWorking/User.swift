//
//  SearchUser.swift
//  preTest17Live
//
//  Created by Eric Chen on 2021/5/26.
//

import Foundation

struct UserRequest {
    var parameters: [String: String]
    private init(parameters: [String: String]) {
        self.parameters = parameters
    }
}

extension UserRequest {
    static func search(keyword: String) -> UserRequest{
        let parameters = ["q": keyword, "per_page": "30"]
        return UserRequest(parameters: parameters)
    }
}

struct UserResponseData: Codable {
    var totalCount: Int
    var incompletResults: Bool
    var users: [User]
    
    enum CodingKeys: String, CodingKey {
        case totalCount = "total_count"
        case incompletResults = "incomplete_results"
        case users = "items"
    }
}

struct User: Codable {
    var login: String
    var id: Int
    var nodeId: String
    var avatarURL: String
    var gravatarId: String
    var url: String
    var htmlURL: String
    var followersURL: String
    var subscriptionsURL: String
    var organizationsURL: String
    var reposURL: String
    var receivedEventsURL: String
    var type: String
    var score: Int
    var followingURL: String
    var gistsURL: String
    var starred_url: String
    var events_url: String
    var siteAdmin: Bool
    
    enum CodingKeys: String, CodingKey {
        case login
        case id
        case nodeId = "node_id"
        case avatarURL = "avatar_url"
        case gravatarId = "gravatar_id"
        case url
        case htmlURL = "html_url"
        case followersURL = "followers_url"
        case subscriptionsURL = "subscriptions_url"
        case organizationsURL = "organizations_url"
        case reposURL = "repos_url"
        case receivedEventsURL = "received_events_url"
        case type
        case score
        case followingURL = "following_url"
        case gistsURL = "gists_url"
        case starred_url = "starred_url"
        case events_url = "events_url"
        case siteAdmin = "site_admin"
    }
}
