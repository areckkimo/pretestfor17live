//
//  GitHubClient.swift
//  preTest17Live
//
//  Created by Eric Chen on 2021/5/27.
//

import Foundation

final class GitHubClient {
    private lazy var baseURL: URL? = {
        return URL(string: "https://api.github.com")
    }()
    let session: URLSession
    
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    func fetchUsers(with request: UserRequest, page: Int = 1, completion: @escaping (Result<(UserResponseData, HTTPURLResponse), HTTPError>) -> Void) {
        
        guard let searchUserURL = URL(string: "/search/users", relativeTo: baseURL) else {
            completion(.failure(.invalidURL))
            return
        }
        
        var urlRequest = URLRequest(url: searchUserURL)
        
        let parameters = ["page": "\(page)"].merging(request.parameters, uniquingKeysWith: +)
        
        urlRequest = urlRequest.queryDataAdapter(data: parameters)
        
        session.dataTask(with: urlRequest) { (data, response, error) in
            
            if let _ = error {
                completion(.failure(.requestFail))
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(.noHttpResponse))
                return
            }
            
            guard httpResponse.isSuccess else {
                completion(.failure(.httpResponseFail))
                return
            }
            
            guard let data = data else{
                completion(.failure(.noData))
                return
            }
            
            let decoder = JSONDecoder()
            
            guard let userResponseData = try? decoder.decode(UserResponseData.self, from: data) else {
                completion(.failure(.jsonDecodingFail))
                return
            }
            
            completion(.success((userResponseData, httpResponse)))
            
        }.resume()
    }
}
